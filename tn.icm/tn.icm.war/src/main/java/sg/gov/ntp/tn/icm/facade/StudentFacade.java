/*
 * Author : Joshua Abellanosa
 */
package sg.gov.ntp.tn.icm.facade;

import java.math.BigDecimal;

import tn.icm.lib.Student;

public interface StudentFacade {

  public Student saveStudent(Student stud);

  public Student getStudent(BigDecimal id);

}

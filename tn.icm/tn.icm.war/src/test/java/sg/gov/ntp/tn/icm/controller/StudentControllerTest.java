package sg.gov.ntp.tn.icm.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import net.minidev.json.JSONObject;
import sg.gov.ntp.tn.icm.facade.StudentFacade;
import sg.gov.ntp.tn.icm.facade.StudentFacadeImpl;
import tn.icm.lib.Student;

@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class StudentControllerTest {

	@Mock
	StudentFacadeImpl studFacadeImpl;

	@Mock
	StudentFacade studFacade;

	@InjectMocks
	StudentController studController;

	private MockMvc mockMvc;

	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(studController).build();
	}

	@Test
	public void saveStudent() throws Exception {

		Mockito.when(studFacade.saveStudent(Mockito.any(Student.class))).thenReturn(new Student());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("studentId", 7);
		jsonObject.put("fname", "joshua");
		jsonObject.put("lname", "abellanosa");
		jsonObject.put("age", 21);
		jsonObject.put("gender", "m");
		jsonObject.put("createdBy", "system");
		jsonObject.put("createdDt", 05/05/2017);
		jsonObject.put("lastUpdatedDt", 05/05/2017);
		jsonObject.put("lastUpdatedBy", "system");
		jsonObject.put("serverName", "SERVER");
		jsonObject.put("systemName", "SYSTEM");
		jsonObject.put("verNo", 1);

		System.out.println(jsonObject.toString());
		MvcResult result = this.mockMvc.perform(post("/ntp/save").content(jsonObject.toJSONString())
				.header("transactionId", "1").header("sessionId", "1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk()).andReturn();
		assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
		Mockito.verify(studFacade).saveStudent(Mockito.any(Student.class));
	}

	@Test
	public void getStudent() throws Exception {

		Mockito.when(studFacade.getStudent(Mockito.any(BigDecimal.class))).thenReturn(new Student());

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("studentId", 7);
		jsonObject.put("fname", "joshua");
		jsonObject.put("lname", "abellanosa");
		jsonObject.put("age", 21);
		jsonObject.put("gender", "m");
		jsonObject.put("createdBy", "system");
		jsonObject.put("createdDt", null);
		jsonObject.put("lastUpdatedDt", null);
		jsonObject.put("lastUpdatedBy", "system");
		jsonObject.put("serverName", "SERVER");
		jsonObject.put("systemName", "SYSTEM");
		jsonObject.put("verNo", 1);

		MvcResult result = this.mockMvc.perform(get("/ntp/student/7").content(jsonObject.toJSONString())
				.header("transactionId", "1").header("sessionId", "1").accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk()).andReturn();
		assertEquals(result.getResponse().getStatus(), HttpStatus.OK.value());
		Mockito.verify(studFacade).getStudent(Mockito.any(BigDecimal.class));
	}

}

package sg.gov.ntp.tn.icm.facade;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import tn.icm.business.service.StudentService;
import tn.icm.lib.Student;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class StudentFacadeImplTest {

	@Mock
	StudentService studService;

	@InjectMocks
	StudentFacadeImpl studFacadeImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void saveStudent() throws Exception {

		Student student = new Student();
		student.setStudentId(new BigDecimal(1));
		student.setFname("joshua");
		student.setLname("abellanosa");
		student.setAge(new BigDecimal(23));
		student.setGender("M");
		student.setCreatedBy("system");
		student.setCreatedDt(null);
		student.setLastUpdatedBy("system");
		student.setLastUpdatedDt(null);
		student.setServerName("SERVER");
		student.setSystemName("SYSTEM");
		student.setVerNo(null);
		studFacadeImpl.saveStudent(student);

		Mockito.when(studService.create(Mockito.any(Student.class))).thenReturn(student);
		studFacadeImpl.saveStudent(Mockito.any(Student.class));
		Mockito.verify(studService).create(Mockito.any(Student.class));
	}

	@Test
	public void getStudent() throws Exception {

		Student student = new Student();
		student.setStudentId(new BigDecimal(1));
		student.setFname("joshua");
		student.setLname("abellanosa");
		student.setAge(new BigDecimal(23));
		student.setGender("M");
		student.setCreatedBy("system");
		student.setCreatedDt(null);
		student.setLastUpdatedBy("system");
		student.setLastUpdatedDt(null);
		student.setServerName("SERVER");
		student.setSystemName("SYSTEM");
		student.setVerNo(null);
		studFacadeImpl.getStudent(new BigDecimal(1));

		Mockito.when(studService.findById(Mockito.any(BigDecimal.class))).thenReturn(student);
		studFacadeImpl.getStudent(Mockito.any(BigDecimal.class));
		Mockito.verify(studService).findById(Mockito.any(BigDecimal.class));
	}

}

package tn.icm.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import java.math.BigDecimal;
import tn.icm.lib.jpa.ClassroomEntity;

/**
 * Repository : Classroom.
 */
public interface ClassroomJpaRepository
    extends PagingAndSortingRepository<ClassroomEntity, BigDecimal> {

}

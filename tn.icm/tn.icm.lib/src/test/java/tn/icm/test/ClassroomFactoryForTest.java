package tn.icm.test;

import java.math.BigDecimal;

import org.junit.Test;

import tn.icm.lib.Classroom;

public class ClassroomFactoryForTest {

	private MockValues mockValues = new MockValues();

	public Classroom newClassroom() {

		BigDecimal classroomId = mockValues.nextBigDecimal();

		Classroom classroom = new Classroom();
		classroom.setClassroomId(classroomId);
		return classroom;
	}

	@Test
	public void classroomFactoryTest(){

	}

}

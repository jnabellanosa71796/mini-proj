package tn.icm.test;

import java.math.BigDecimal;

import org.junit.Test;

import tn.icm.lib.jpa.StudentEntity;

public class StudentEntityFactoryForTest {

	private MockValues mockValues = new MockValues();

	public StudentEntity newStudentEntity() {

		BigDecimal studentId = mockValues.nextBigDecimal();

		StudentEntity studentEntity = new StudentEntity();
		studentEntity.setStudentId(studentId);
		return studentEntity;
	}

	@Test
	public void studentEntityTest(){

	}

}

package tn.icm.test;

import java.math.BigDecimal;

import org.junit.Test;

import tn.icm.lib.jpa.ClassroomEntity;

public class ClassroomEntityFactoryForTest {

	private MockValues mockValues = new MockValues();

	public ClassroomEntity newClassroomEntity() {

		BigDecimal classroomId = mockValues.nextBigDecimal();

		ClassroomEntity classroomEntity = new ClassroomEntity();
		classroomEntity.setClassroomId(classroomId);
		return classroomEntity;
	}

	@Test
	public void classroomEntityTest(){

	}
}
